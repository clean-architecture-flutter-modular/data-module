import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart';

import 'package:domain/domain.dart';

import '../patterns/http/stretegy/context/context_http.dart';

part 'failure.dart';
part 'logs.dart';
part 'parse_response_body.dart';

enum HttpMethod { get, post, patch, delete, put }

class Http {
  Http({
    required Client client,
    required String baseUrl,
    required String apiKey,
  })  : _client = client,
        _baseUrl = baseUrl,
        _apiKey = apiKey;

  final Client _client;
  final String _baseUrl;
  final String _apiKey;

  Future<Either<HttpFailure, R>> request<R>(
    String path, {
    required R Function(dynamic responseBody) onSuccess,
    HttpMethod method = HttpMethod.get,
    Map<String, String> headers = const {},
    Map<String, String> queryParameters = const {},
    Map<String, dynamic> body = const {},
    bool useApiKey = true,
    String languageCode = 'en',
    Duration timeout = const Duration(seconds: 10),
  }) async {
    Map<String, dynamic> logs = {};
    StackTrace? stackTrace;
    try {
      if (useApiKey) {
        queryParameters = {
          ...queryParameters,
          'api_key': _apiKey,
        };
      }
      Uri url = parseUri(path);

      if (queryParameters.isNotEmpty) {
        url = urlReplace(url, languageCode, queryParameters);
      }

      headers = {
        'Content-Type': 'application/json',
        ...headers,
      };

      final String bodyString = jsonEncode(body);
      logs = {
        'url': url.toString(),
        'method': method.name,
        'body': body,
      };

      ContextHttp context =
          ContextHttp(bodyString, headers, client: _client, url: url);
      context.setStrategy();
      context.chooseStrategy(method.name);

      final Response response = await context.getAction();

      final statusCode = response.statusCode;
      final responseBody = _parseResponseBody(
        response.body,
      );
      logs = {
        ...logs,
        'startTime': DateTime.now().toString(),
        'statusCode': statusCode,
        'responseBody': responseBody,
      };

      if (statusCode >= 200 && statusCode < 300) {
        return Either.right(
          onSuccess(
            responseBody,
          ),
        );
      }

      return Either.left(
        HttpFailure(
          statusCode: statusCode,
          data: responseBody,
        ),
      );
    } catch (e, s) {
      stackTrace = s;

      logs['exception'] = e.toString();

      if (e is SocketException || e is ClientException) {
        logs['exception'] = 'NetworkException';
        return Either.left(HttpFailure(exception: NetworkException()));
      }

      return Either.left(HttpFailure(exception: e));
    } finally {
      _printLogs({...logs, 'endTime': DateTime.now().toString()}, stackTrace);
    }
  }

  Uri parseUri(String path) {
    Uri url = Uri.parse(
      path.startsWith('http') ? path : '$_baseUrl$path',
    );

    return url;
  }

  Uri urlReplace(
      Uri url, String languageCode, Map<String, String> queryParameters) {
    return url.replace(
      queryParameters: {
        ...queryParameters,
        'language': languageCode,
      },
    );
  }
}
