import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart';

class InternetChecker {
  Future<bool> hasInternet() async {
    try {
      if (kIsWeb) {
        return (await get(Uri.parse('8.8.8.8'))).statusCode == 200;
      } else {
        final result = await InternetAddress.lookup('8.8.8.8');
        return result.isNotEmpty && result[0].rawAddress.isNotEmpty;
      }
    } on SocketException catch (_) {
      return false;
    }
  }
}
