import 'package:domain/domain.dart';

import '../../http/http.dart';
import '../../utils/handle_failure.dart';
import '../local/language_service.dart';

class TrendingAPI {
  TrendingAPI(
    this._http,
    this._languageService,
  );
  final Http _http;
  final LanguageService _languageService;

  Future<Either<HttpRequestFailure, List<Media>>> getMoviesAndSeries(
    TimeWindow timeWindow,
  ) async {
    final result = await _http.request(
      '/trending/all/${timeWindow.name}',
      languageCode: _languageService.languageCode,
      onSuccess: (json) {
        final list = List<Json>.from(json['results'] as List<dynamic>);
        return getMediaList(list);
      },
    );
    return result.when(
      left: handleHttpFailure,
      right: (list) => Either.right(list),
    );
  }

  Future<Either<HttpRequestFailure, List<Performer>>> getPerformers(
    TimeWindow timeWindow,
  ) async {
    final result = await _http.request(
      '/trending/person/${timeWindow.name}',
      languageCode: _languageService.languageCode,
      onSuccess: (json) {
        final list = List<Json>.from(json['results'] as List<dynamic>);
        return list
            .where(
              (e) =>
                  e['known_for_department'] == 'Acting' &&
                  e['profile_path'] != null,
            )
            .map(
              (e) => Performer.fromJson(e),
            )
            .toList();
      },
    );
    return result.when(
      left: handleHttpFailure,
      right: (list) => Either.right(list),
    );
  }
}
