import 'package:http/http.dart';

import '../stretegyManager/http_method_strategy.dart';

class DeleteStrategy implements HttpMethodStrategy {
  @override
  Future<Response> execute(
    Client client,
    Uri url, {
    Map<String, String>? headers,
    String? body,
    Duration timeout = const Duration(seconds: 5),
  }) {
    return client.delete(url, headers: headers).timeout(timeout);
  }
}
