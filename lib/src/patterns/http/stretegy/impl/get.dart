import 'package:http/http.dart';

import '../stretegyManager/http_method_strategy.dart';

class GetStrategy implements HttpMethodStrategy {
  @override
  Future<Response> execute(
    Client client,
    Uri url, {
    Map<String, String>? headers,
    String? body,
    Duration timeout = const Duration(seconds: 5),
  }) {
    return client.get(url).timeout(timeout);
  }
}
