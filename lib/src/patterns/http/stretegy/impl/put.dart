import 'package:http/http.dart';

import '../stretegyManager/http_method_strategy.dart';

class PutStrategy implements HttpMethodStrategy {
  @override
  Future<Response> execute(
    Client client,
    Uri url, {
    Map<String, String>? headers,
    String? body,
    Duration timeout = const Duration(seconds: 5),
  }) {
    return client.put(url, headers: headers, body: body).timeout(timeout);
  }
}
