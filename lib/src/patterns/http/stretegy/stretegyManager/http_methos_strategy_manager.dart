import 'http_method_strategy.dart';

class HttpMethodStrategyManager {
  final Map<String, HttpMethodStrategy> _strategies = {};

  void addStrategy(String key, HttpMethodStrategy strategy) {
    _strategies[key] = strategy;
  }

  HttpMethodStrategy? getStrategy(String key) {
    return _strategies[key];
  }
}
