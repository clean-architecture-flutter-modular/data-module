import 'package:http/http.dart';

abstract class HttpMethodStrategy {
  Future<Response> execute(
    Client client,
    Uri url, {
    Map<String, String>? headers,
    String? body,
    Duration timeout = const Duration(seconds: 5),
  });
}
