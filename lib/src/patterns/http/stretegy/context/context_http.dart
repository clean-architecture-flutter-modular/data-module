import 'package:http/http.dart';

import '../impl/delete.dart';
import '../impl/get.dart';
import '../impl/patch.dart';
import '../impl/post.dart';
import '../impl/put.dart';
import '../stretegyManager/http_method_strategy.dart';
import '../stretegyManager/http_methos_strategy_manager.dart';

class ContextHttp {
  ContextHttp(
    String body,
    Map<String, String>? headers, {
    required Client client,
    required Uri url,
  })  : _client = client,
        _url = url,
        _headers = headers,
        _body = body;

  final _strategyManager = HttpMethodStrategyManager();
  late HttpMethodStrategy strategy;
  final Client _client;
  final Uri _url;
  final Map<String, String>? _headers;
  final String _body;

  void setStrategy() {
    _strategyManager.addStrategy('post', PostStrategy());
    _strategyManager.addStrategy('get', GetStrategy());
    _strategyManager.addStrategy('patch', PatchStrategy());
    _strategyManager.addStrategy('put', PutStrategy());
    _strategyManager.addStrategy('delete', DeleteStrategy());
  }

  void chooseStrategy(String key) {
    strategy = _strategyManager.getStrategy(key)!;
  }

  Future<Response> getAction() {
    return strategy.execute(_client, _url, headers: _headers, body: _body);
  }
}
