import 'package:domain/domain.dart';

import '../services/local/language_service.dart';

class LanguageRepositoryImpl implements LanguageRepository {
  LanguageRepositoryImpl(this._service);
  final LanguageService _service;

  @override
  void setLanguageCode(String code) {
    _service.setLanguageCode(code);
  }
}
