import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:domain/domain.dart';

import '../services/remote/internet_checker.dart';

class ConnectivityImpl implements ConnectivityRepository {
  ConnectivityImpl(this._connectivity, this._internetChecker);
  final Connectivity _connectivity;
  final InternetChecker _internetChecker;
  final _controller = StreamController<bool>.broadcast();

  late bool _hasInternet;
  // ignore: strict_raw_type
  StreamSubscription? _subscription;

  @override
  bool get hasInternetConnection => _hasInternet;

  @override
  Future<void> initialize() async {
    Future<bool> hasInternet(
        List<ConnectivityResult> connectivityResult) async {
      if (connectivityResult.contains(ConnectivityResult.none)) {
        return false;
      }
      return _internetChecker.hasInternet();
    }

    _hasInternet = await hasInternet(
      await _connectivity.checkConnectivity(),
    );

    _connectivity.onConnectivityChanged.listen(
      (event) async {
        _subscription?.cancel();
        _subscription = hasInternet(event).asStream().listen(
          (value) {
            _hasInternet = value;

            if (_controller.hasListener && !_controller.isClosed) {
              _controller.add(_hasInternet);
            }
          },
        );
      },
    );
  }

  @override
  // TODO: implement onInternetChanged
  Stream<bool> get onInternetChanged => _controller.stream;
}
